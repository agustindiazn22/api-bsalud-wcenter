"use strict";
const moment = require('moment')
moment.locale('es')

const axios = require('axios').default;
const config = require("../config");

var Transaction;
var Company;
var MovementOfCash;
var MovementOfArticle;
var Employe;
var Bank;
initConnectionDB('bsalud')

async function saveArticle(documentos, token) {
    // console.log(documentos.cpEmpresa + documentos.cpSucursal + documentos.idCompra)
    // console.log('-------------------------')

    var res;
    var idTran = (documentos.cpEmpresa +'-'+ documentos.cpSucursal +'-'+ documentos.idCompra).toString()
    let transactionIdem = await getAllTransaction(token,idTran )
    if (transactionIdem && transactionIdem.status == 200 && transactionIdem.data.result.length > 0) {
        res = {
            status: 200,
            res: false,
            err: 'Existe',
            code: idTran

        }
    } else {
        if (documentos.dcDocumento != 'RC') {
            let taxes = [];

            var transaction = new Transaction();

            // return documentos.dcNumero

            transaction.wooId = idTran

            transaction.origin = parseInt(documentos.cpPuesto);
            transaction.letter = documentos.cpLetra;
            transaction.number = parseInt(documentos.cpNumero);
            transaction.exempt = parseFloat(documentos.cpExento);

            transaction.totalPrice = parseFloat(documentos.cpTotal)

            
            transaction.balance = parseFloat(documentos.cpTotal);
            // transaction.balance = 0
            transaction.state = 'Cerrado';
            transaction.madein = 'mostrador';
            // solo factura compra
            transaction.type = '6059d3e49ebd8104633f6f88';
            
            transaction.startDate = Date.now();
            transaction.CAEExpirationDate = documentos.cpVence;

            // let f = moment(documentos.dcFecha, 'YYYY-MM-DD').add(documentos.dcHora.split(':')[0], 'h').add(documentos.dcHora.split(':')[1], 'm').format('YYYY-MM-DDTHH:mm:ssZ');
            // let f = moment(documentos.cpFecha, 'YYYY-MM-DD').format('YYYY-MM-DDTHH:mm:ssZ');

            var fechaMañana = documentos.dcFecha;
            fechaMañana=fechaMañana.setDate(fechaMañana.getDate() + 1);

            transaction.endDate = fechaMañana;

            transaction.branchOrigin = '6059d3e49ebd8104633f6f97';
            transaction.branchDestination = '6059d3e49ebd8104633f6f97';

            transaction.quotation = 1;

            transaction.depositDestination = '6059d3e49ebd8104633f6f98';
            transaction.depositOrigin = '6059d3e49ebd8104633f6f98';

            // CPIVA
            if (documentos.cpIva > 0 && documentos.cpGravado > 0) {
                taxes.push(await taxesGenerate(documentos.cpGravado, documentos.cpIva, documentos.cpTasa))
            }
            if (documentos.cpIva1 > 0 && documentos.cpGravado1 > 0) {
                taxes.push(await taxesGenerate(documentos.cpGravado1, documentos.cpIva1, documentos.cpTasa1))
            }
            if (documentos.cpRIBrutos > 0) {
                taxes.push(await taxesGenerate(documentos.cpRIBrutos, 0, 'IIBB'))
            }
            if (documentos.cpRIva > 0) {
                taxes.push(await taxesGenerate(documentos.cpRIva, 0, 'RIVA'))
            }

            transaction.taxes = taxes

            transaction.VATPeriod = await VATPeriodGenerate(documentos.cpFecha);


            if (documentos.cpidProveedor == 99999999 || documentos.cpidProveedor == null || documentos.cpidProveedor == ' ' || documentos.cpidProveedor == 'FINAL CONSUMIDOR' || documentos.prNombre == '') {
                //traer cliente consumidor final del sistema poscloud companies 
                transaction.company = '6059d3e39ebd8104633f6f6f'
            } else {
                var comaniesCFIdem = await getComaniesCF(token, documentos.cpidProveedor)

                if (comaniesCFIdem && comaniesCFIdem.status == 200 && comaniesCFIdem.data.result.length > 0) {
                    transaction.company = comaniesCFIdem.data.result[0]._id

                } else {
                    var company = new Company();
                    company.code = 99;
                    company.allowCurrentAccount = false;
                    company.name = documentos.prNombre;
                    company.type = 'Proveedor';
                    company.entryDate = Date.now()
                    company.wooId = documentos.cpidProveedor

                    if (!documentos.prCuit) {
                        company.identificationValue = '99999999'
                    } else {
                        company.identificationValue = documentos.prCuit
                    }

                    if (documentos.prCuit) {
                        company.identificationType = '6059d3e39ebd8104633f6f6b'
                    }
                    else {
                        company.identificationType = '6059d3e39ebd8104633f6f6c'
                    }

                    if (documentos.prIva == 1) {
                        company.vatCondition = '6059d3e39ebd8104633f6f66'
                    } else if (documentos.prIva == 2) {
                        company.vatCondition = '6059d3e39ebd8104633f6f67'
                    } else if (documentos.prIva == 3) {
                        company.vatCondition = '60dbd3d94aad46356d9181a2'
                    } else if (documentos.prIva == 4) {
                        company.vatCondition = '6059d3e39ebd8104633f6f68'
                    } else if (documentos.prIva == 5) {
                        company.vatCondition = '6059d3e39ebd8104633f6f69'
                    } else if (documentos.prIva == 6) {
                        company.vatCondition = '6059d3e39ebd8104633f6f6a'
                    } else {
                        company.vatCondition = '6059d3e39ebd8104633f6f66'
                    }
                    company.operationType = 'C';

                    company.account = '60f1c059f31e514d26e3dc05'
                    transaction.company = await saveCompany(company, token)
                }
            }

            await saveTransaction(transaction, token)
                .then(r => {
                    // console.log(r)
                    res = {
                        status: 200,
                        res: r,
                        err: false,
                        code: idTran
                    }
                })
                .catch(e => {
                    res = {
                        status: 500,
                        res: false,
                        err: e,
                        code: idTran
                    }
                })
            // console.log(res.res)
            var transactionSave = res.res.data.result
            var transactionId = res.res.data.result._id

            if (!res.err && transactionId) {

                var totalPrice = res.res.data.result.totalPrice;

                // Solo Cuenta Corriente
                var move = new MovementOfCash();
                move.operationType = 'C';
                move.creationDate = Date.now();
                move.date = Date.now();
                // move.expirationDate = documentos.dcFechaAlta;
                move.expirationDate = documentos.cpFecha
                move.transaction = transactionId;
                move.type = '6059d3e39ebd8104633f6f72'; //Cuenta corriente
                move.amountPaid = totalPrice;
                move.state = 'Cerrado';

                await generateMoveCash(move, token).catch(e => {
                    res = {
                        status: 500,
                        res: false,
                        err: e,
                        code: idTran
                    }
                });



            }

            await seating(transactionId, token).catch(e => {
                res = {
                    status: 500,
                    res: false,
                    err: e,
                    code: idTran
                }
            })


            if (!transactionId) {
                res = {
                    status: 500,
                    res: false,
                    err: 'transactionId',
                    code: idTran
                }
            }
        } else {
            // console.log('asd')
            res = {
                status: 200,
                res: false,
                err: 'Existe',
                code: idTran
            }
            if (documentos.dcDocumento == 'RC') {
                res = {
                    status: 200,
                    res: false,
                    err: 'RC',
                    code: idTran
                }
            }
        }
    }

    return res;

}

async function VATPeriodGenerate(fecha) {
    let fechaVat = fecha.toISOString().substr(0, 4) + fecha.toISOString().substr(5, 2);
    return fechaVat;
}

async function generateMoveCash(move, token) {
    return new Promise((resolve, reject) => {
        axios.post(
            config.url + 'movements-of-cashes',
            move,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }
        ).then(r => {
            resolve(r)
        }).catch(e => {
            reject(e)
        })
    })
}

async function getAllTransaction(token, wFarmaId) {
    return new Promise((resolve, reject) => {
        axios.get(
            config.url + 'transactions/',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                params:
                {
                    project: {
                        '_id': 1,
                        'wooId': 1,
                        'operationType': 1,
                    },
                    match: {
                        'operationType': { $ne: 'D' },
                        'wooId': wFarmaId.toString()
                    }
                }
            }
        ).then((res) => {
            resolve(res)
        }).catch((e) => {
            reject(e)
        })
    })
}

async function findBank(bankCode, token) {
    return new Promise((resolve, reject) => {
        axios.get(
            config.url + 'banks',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                params:
                {
                    project: {
                        '_id': 1,
                        'code': 1,
                        'operationType': 1,
                    },
                    match: {
                        'operationType': { $ne: 'D' },
                        'code': bankCode
                    }
                }
            }
        ).then((res) => {

            resolve(res)
        }).catch((e) => {
            // console.log('error transaction axios: ', e)
            reject(e)
        })
    })
}
async function getComaniesCF(token, cliente) {

    return new Promise((resolve, reject) => {
        axios.get(
            config.url + 'companies/',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },

                params:
                {
                    project: {
                        '_id': 1,
                        'wooId': 1,
                        'operationType': 1,
                    },
                    match: {
                        'operationType': { $ne: 'D' },
                        'wooId': cliente.toString()
                    }
                }
            }
        ).then((res) => {

            resolve(res)
        }).catch((e) => {
            // console.log('error transaction axios: ', e)
            reject(e)
        })
    })
}
async function saveBank(bank, token) {
    return new Promise((resolve, reject) => {
        axios.post(
            config.url + 'banks/',
            bank,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }
        ).then(function (res) {
            resolve(res.data.result._id)
        }).catch(function (e) {

            reject(e)
        })
    })
}


async function saveCompany(company, token) {
    return new Promise((resolve, reject) => {
        axios.post(
            config.url + 'companies/',
            company,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }
        ).then(function (res) {
            resolve(res.data.result._id)
        }).catch(function (e) {
            // console.log('error transaction axios: ', e)
            reject(e)
        })
    })
}

async function saveTransaction(transaction, token) {
    return new Promise((resolve, reject) => {
        axios.post(
            config.url + 'transactions',
            transaction
            , {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }
        ).then(async res => {
            // console.log('res transaction axios: ',res)
            resolve(res)
        }).catch(function (e) {
            // console.log('error transaction axios: ', e)
            reject(e)
        })
    })
}
async function taxesGenerate(gravado, iva, type) {
    let taxes = [];
    if (type == 21) {
        taxes =
        {
            "tax": "6059d3e39ebd8104633f6f77",
            "percentage": 21,
            "taxBase": gravado,
            "taxAmount": iva
        }
    } else if (type == 10.50) {
        taxes =
        {
            "tax": "6059d3e39ebd8104633f6f78",
            "percentage": 10.5,
            "taxBase": gravado,
            "taxAmount": iva
        }
    } else if (type == 'IIBB') {
        taxes =
        {
            "tax": "60f859be8d946b4d09031f84",
            "percentage": 0,
            "taxBase": iva,
            "taxAmount": gravado
        }
    }
    else if (type == 'RIVA') {
        taxes =
        {
            "tax": "6059d3e39ebd8104633f6f7a",
            "percentage": 0,
            "taxBase": iva,
            "taxAmount": gravado
        }
    }


    // console.log('taxes Taces: ', taxes)
    return taxes
}

async function seating(id, token) {
    return new Promise((resolve, reject) => {
        axios.post(
            config.url + 'account-seat-transaction/' + id,
            '',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }
        ).then((res) => {
            resolve(res)
        }).catch((e) => {
            reject(e)
        })
    })
}

function initConnectionDB(database) {

    const Model = require('../models/model');

    var TransactionSchema = require('../models/transaction');
    Transaction = new Model('transaction', {
        schema: TransactionSchema,
        connection: database
    });

    var CompanySchema = require('../models/company');
    Company = new Model('company', {
        schema: CompanySchema,
        connection: database
    });

    var MovementOfCashSchema = require('../models/movement-of-cash');
    MovementOfCash = new Model('movementOfCash', {
        schema: MovementOfCashSchema,
        connection: database
    });

    var BankSchema = require('../models/bank');
    Bank = new Model('bank', {
        schema: BankSchema,
        connection: database
    })
}

module.exports = {
    saveArticle,
    seating
}