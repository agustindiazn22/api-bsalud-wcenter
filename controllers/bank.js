"use strict";
const axios = require('axios').default;
const config = require("../config");
var Bank;
initConnectionDB('bsalud')

async function getAllBank(token) {
    return new Promise((resolve, reject) => {
        axios.get(
            config.url + 'banks/',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                params:
                {
                    limit: 100000
                }
            }
        ).then((res) => {
            // console.log(res.data.result)
            resolve(res.data.result)
        }).catch((e) => {
            reject(e)
        })
    })
}

async function saveBank(docBank, banks, token) {
    var codeArrayBanks = [];
    for (let i = 0; i < banks.length; i++) {
        codeArrayBanks.push(banks[i].code)
    }

    if (!codeArrayBanks.includes(docBank.idOSocial)) {
        // Crear bank
        var bank = new Bank();
        bank.code = docBank.idOSocial;
        bank.agency = 1;
        bank.name = docBank.osNombre;
        bank.creationDate = Date.now();

        let res = await axios.post(
            config.url + 'banks',
            bank
            , {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }
        )
       return res;

    } else {
        let res = {
            status:500,
            id: docBank.idOSocial
        }
        return res
        // Existe
    }


}





function initConnectionDB(database) {

    const Model = require('../models/model');

    var BankSchema = require('../models/bank');
    Bank = new Model('bank', {
        schema: BankSchema,
        connection: database
    });
}

module.exports = {
    getAllBank,
    saveBank
}