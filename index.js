"use strict";

const axios = require('axios').default;
const config = require("./config");
const sqlConnection = require("./sql");
const sql = new sqlConnection(config.config);
var cron = require('node-cron');

const Transaction = require("./controllers/transaction");
const Bank = require("./controllers/bank");

(async () => {
    try {

        var CronJob = require('cron').CronJob;

        var job = new CronJob('0 0 4 * * *', function () {
            sincronizacion();
        }, null, true);
        job.start();

        // sincronizacion()
        async function sincronizacion() {

            let hoy = new Date();
            let DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
            let ayer = new Date(hoy.getTime() - DIA_EN_MILISEGUNDOS);
            let hora = ayer.getHours()
            // 20210601
            let año = ayer.getFullYear()
            var m = ayer.getMonth() + 1;
            var mes = (m < 10) ? '0' + m : m;
            let d = ayer.getDate()
            var dia = (d < 10) ? '0' + d : d;

            let fecha = año.toString() + mes.toString() + dia.toString()

            var token = await config.getTokenBsalud().catch(e => {
                console.log(e)
            })

            // let desde = '20190801'
            // let hasta = '20200729'

            // Hace la consulta con la fecha de ayer
            let doc = await sql.bsalud(fecha);

            // hace la consulta con la fecha indicada 
            // let doc = await sql.bsalud(desde, hasta);

            let documentos = doc.recordset;


            // excel(documentos)
            let array200 = []
            let array500 = []
            let arrayExi = []
            // console.log(allTransactions.length)
            if (documentos.length > 0) {
                for (let i = 0; i < documentos.length; i++) {
                    // console.log('documentos: ',documentos[i].cpFecha)

                    let tran = await Transaction.saveArticle(documentos[i], token)
                    //     console.log(i)
                    if (tran.status == 200) {
                        array200.push(tran.code)
                    }
                    if (tran.status == 500) {
                        array500.push(tran.code)
                        console.log('err: ', tran.err)
                    }
                    if (tran.err == 'Existe') {
                        arrayExi.push(tran.code)
                    }
                    // console.log('tran: ', tran.status)
                    // console.log('err: ', tran.err)
                    // console.log('------------------------')
                    if (i + 1 == documentos.length) {
                        console.log('FINISHHHH')
                        console.log('------------array200------------')
                        console.log(array200.length)
                        console.log(array200)
                        console.log('------------array500------------')
                        console.log(array500.length)
                        console.log(array500)
                        console.log('------------arrayExi------------')
                        console.log(arrayExi.length)
                        console.log(arrayExi)
                    }

                }
            } else {
                console.log('doc undefined')
            }
        }

    } catch (error) {
        sql.close();
        console.log(error);
    }

})();




